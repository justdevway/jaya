(function($) {
  $.fn.makeParallax = function(){
  var parallaxChildren = $(this).find("[data-y], [data-x]"),
      parallaxOffsetTop = $(this).offset().top,
      currentWidth = $(document).width(),
      parallaxHeight = $(this).height(),
      parallaxBackground = $(this).find("[data-type='background']"),
      parallaxBackgroundSrc = parallaxBackground.attr("src"),
      parallaxOffsetTop2 = $(this).offset().top;
    
//    $(this).css({
//          "background-image": "url('" + parallaxBackgroundSrc + "')",
//          "background-repeat": "no-repeat",
//          "background-size": "cover",
//          "backgorund-attachement": "fixed",
//    });
    
      
    
    if (currentWidth >= 1024) {
      parallaxOffsetTop -= 400;
    } else if (currentWidth < 1024 && currentWidth > 480) {
      parallaxOffsetTop -= 100;
    } else if (currentWidth < 481) {
      parallaxOffsetTop -= 100;
    }
    console.log("currentWidth + " + currentWidth);
    $(this).css({
      //      "background-attachment": "fixed",
      "overflow": "hidden"
    })
    parallaxChildren.each(function () {
      var workElem = $(this);
      $(window).scroll(function () {
        var scroll = $(this).scrollTop();
        if (scroll > parallaxOffsetTop) {
          var workScrolling = scroll - parallaxOffsetTop,
            elementYPosition = workScrolling * workElem.data("y") * 0.1,
            elementXPosition = workScrolling * workElem.data("x") * 0.02;
          if (elementXPosition && elementYPosition) {
            workElem.css({
              "transition": "all 0.1s linear",
              "transform": "translate(" + elementXPosition + "%, " + elementYPosition + "%)"
              //              "background" : "orange"
            })
          } else if (elementXPosition) {
            workElem.css({
              "transition": "all 0.1s linear",
              "transform": "translate(" + elementXPosition + "%, " + "0%)"
            })
          } else if (elementYPosition) {
            workElem.css({
               "transition": "all 0.1s linear",
               "transform": "translate(0%, " + elementYPosition + "%)"
            })
          }

        } else {
          workElem.css({
            "transition": "all 0.1s linear",
            "transform": "translate(0%, " + 0 + "%)"
          })
        }
      })
    })
     
    console.log("ParallaxHeight +" + parallaxHeight);
    console.log("parallaxOffsetTop2 +" + parallaxOffsetTop2);



      parallaxBackground.each(function () {
        var workElement = $(this),
            parallaxBackgroundSrc = $(this).attr("src");
        
        workElement.css({
          "width": "100%",
          "height": "100%",
//          "background-image": "url('" + parallaxBackgroundSrc + "')",
//          "background-repeat": "no-repeat",
//          "background-size": "cover",
//          "backgorund-attachement": "fixed",
          "will-change": "transform",
          "position": "absolute",
          "z-index": "-1",
          "bottom": "0",
          "top": "0"
        });
       
        $(window).scroll(function () {
          var scroll = $(this).scrollTop(),
              parallaxCoefficient = scroll / parallaxOffsetTop2;
          console.log("ParalaxCoefficient + " + parallaxCoefficient);
        workElement.css({
          "top": "-100%"
        });
          if(scroll < parallaxOffsetTop2) {
             workElement.css({
//                 "transform": "translate(0, " + ( scroll / (parallaxOffsetTop2)) * 100 + "%)"
               
               "transform": "translate(0, " + parallaxCoefficient * ( scroll / (parallaxOffsetTop2)) * 100 + "%)"
            })
          } 
          else if (scroll > (parallaxOffsetTop2 * 0.1)) {
//            alert("go");
             workElement.css({
               "transform": "translate(0, " + ( scroll / (parallaxOffsetTop2)) * 100 + "%)"
            })
          }
        })
      })
    
     return this;
  };
})(jQuery);
































