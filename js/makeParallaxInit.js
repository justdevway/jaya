$(document).ready(function () {
  $('.slick').slick({
    dots: true,
    dotsClass: "slick__dots",
    arrows: false,
    infinite: true,
    speed: 300,
    autoplay: true
  });
  
  $("#parallax").makeParallax();
  $("#parallax2").makeParallax();
})
